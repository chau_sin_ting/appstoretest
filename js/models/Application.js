/**
 * @flow
 */

export type Label = {
  label: string
};

export type Application = {
  'im:name': Label,
  'im:image': Array<Label>,
  'im:artist': Label,
  summary: Label,
  id: {
    attributes: {
      'im:id': string
    }
  },
  category: {
    attributes: Label
  },
  description: ?string,
  averageUserRating: number,
  userRatingCount: number
};
