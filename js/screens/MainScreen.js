/**
*
* @flow
*/

import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import type { Application } from '../models/Application';
import SearchBar from '../components/SearchBar';
import AppListView from '../components/AppListView';
import BadNetworkView from '../components/BadNetworkView';
import { fetchTopFreeApps } from '../actions/topFreeApps';
import { fetchTopGrossingApps } from '../actions/topGrossingApps';

type Props = {
  topFreeApps: Array<Application>,
  topGrossingApps: Array<Application>,
  fetchTopFreeAppsFailed: boolean,
  fetchTopGrossingFailed: boolean,
  fetchTopFreeApps: () => void,
  fetchTopGrossingApps: () => void
};

type State = {
  keyword: string,
  searchedTopFreeApps: Array<Application>,
  searchedTopGrossingApps: Array<Application>
};

class MainScreen extends React.Component {
  state: State;

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      keyword: '',
      searchedTopFreeApps: [],
      searchedTopGrossingApps: []
    };
  }

  componentDidMount() {
    this.fetchApps();
  }

  render() {
    return !this.props.fetchTopGrossingFailed &&
    !this.props.fetchTopFreeAppsFailed
      ? <View style={styles.container}>
          <SearchBar onChangeText={this.onChangeText} />
          <AppListView
            topFreeApps={
              this.state.keyword.length === 0
                ? this.props.topFreeApps
                : this.state.searchedTopFreeApps
            }
            topGrossingApps={
              this.state.keyword.length === 0
                ? this.props.topGrossingApps
                : this.state.searchedTopGrossingApps
            }
            refreshing={this.props.loadingTopFree}
            onRefresh={this.fetchApps}
          />
        </View>
      : <BadNetworkView onRetry={this.fetchApps.bind(this)} />;
  }

  fetchApps = () => {
    console.log('retry')
    this.props.fetchTopFreeApps();
    this.props.fetchTopGrossingApps();
  };

  onChangeText = (keyword: string) => {
    setTimeout(() => {
      this.setState({
        keyword: keyword,
        searchedTopFreeApps: this.filterApps(this.props.topFreeApps, keyword),
        searchedTopGrossingApps: this.filterApps(
          this.props.topGrossingApps,
          keyword
        )
      });
    }, 500);
  };

  filterApps(apps: Array<Application>, keyword: string): Array<Application> {
    return apps.filter(
      app =>
        app['im:name'].label.includes(keyword) ||
        app.summary.label.includes(keyword) ||
        app.category.attributes.label.includes(keyword) ||
        app['im:artist'].label.includes(keyword)
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  }
});

function select(store, props) {
  return {
    loadingTopFree: store.topFreeApps.loading,
    topFreeApps: store.topFreeApps.apps,
    topGrossingApps: store.topGrossingApps.apps,
    fetchTopFreeAppsFailed: store.topFreeApps.failed,
    fetchTopGrossingFailed: store.topGrossingApps.failed
  };
}

function actions(dispatch, props) {
  return {
    fetchTopFreeApps: () => dispatch(fetchTopFreeApps()),
    fetchTopGrossingApps: () => dispatch(fetchTopGrossingApps())
  };
}

module.exports = connect(select, actions)(MainScreen);
