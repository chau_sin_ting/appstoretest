/**
 * @flow
 */
'use strict';

import { combineReducers } from 'redux';

// import locale from './locale';
import topFreeApps from './topFreeApps';
import topGrossingApps from './topGrossingApps';

export default combineReducers({
  topFreeApps,
  topGrossingApps
});
