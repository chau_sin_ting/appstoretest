/**
 * @flow
 */
'use strict';

import type { Action } from '../actions/types';
import type { Application } from '../models/Application';

export type State = {
  loading: boolean,
  failed: boolean,
  apps: ?Array<Application>
};

const initialState = {
  loading: false,
  failed: false,
  apps: null
};

function topGrossingApps(state: State = initialState, action: Action): State {
  if (action.type === 'LOADING_TOP_GROSSING') {
    return {
      ...state,
      loading: true,
      failed: false
    };
  }
  if (action.type === 'GOT_TOP_GROSSING') {
    return {
      ...state,
      loading: false,
      apps: action.apps
    };
  }
  if (action.type === 'GET_TOP_GROSSING_FAILED') {
    return {
      ...state,
      loading: false,
      failed: true
    };
  }
  return state;
}

module.exports = topGrossingApps;
