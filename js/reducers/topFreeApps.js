/**
 * @flow
 */
'use strict';

import type { Action } from '../actions/types';
import type { Application } from '../models/Application';

export type State = {
  loading: boolean,
  failed: boolean,
  apps: ?Array<Application>
};

const initialState = {
  loading: false,
  failed: false,
  apps: null
};

function topFreeApps(state: State = initialState, action: Action): State {
  if (action.type === 'LOADING_TOP_FREE') {
    return {
      ...state,
      loading: true,
      failed: false
    };
  }
  if (action.type === 'GOT_TOP_FREE') {
    return {
      ...state,
      apps: action.apps,
      loading: false
    };
  }

  if (action.type === 'GETT_TOP_FREE_FAILED') {
    return {
      ...state,
      loading: false,
      failed: true
    };
  }
  return state;
}

module.exports = topFreeApps;
