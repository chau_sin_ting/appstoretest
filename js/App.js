/**
*
* @flow
*/

import { StackNavigator } from 'react-navigation';
import MainScreen from './screens/MainScreen';

const App = StackNavigator({
  Main: { screen: MainScreen }
});

module.exports = App;
