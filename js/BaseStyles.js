import { Platform } from 'react-native';

export default {
  //dimens
  statusBarHeight: Platform.OS === 'ios' ? 18 : 0,
  actionBarHeight: Platform.OS === 'ios' ? 64 : 56,
  searchBarHeight: 35,
  xsmallFont: 6,
  smallFont: 10,
  mediumFont: 12,
  largeFont: 16,
  line: 0.5,
  layoutPadding: 12,
  smallPadding: 6,

  //colors
  gray: '#E4E5E6',
  lightGray: '#F6F6F6',
  darkGray: '#2A2A2A'
};
