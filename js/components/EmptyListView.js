/**
 * @flow
 */

'use strict';

import React from 'react';
import { StyleSheet, Dimensions,View, Text, ActivityIndicator } from 'react-native';
import BaseStyles from '../BaseStyles';

type Props = {
  refreshing: boolean
};

class EmptyListView extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.refreshing
          ? <ActivityIndicator />
          : <Text style={styles.emptyText}>No result</Text>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height - BaseStyles.actionBarHeight,
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    fontSize: BaseStyles.largeFont,
    color: BaseStyles.darkGray,
    textAlign: 'center'
  }
});

module.exports = EmptyListView;
