/**
 * @flow
 */

'use strict';

import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import BaseStyles from '../BaseStyles';
import type { Application } from '../models/Application';
import StarRating from 'react-native-star-rating';

type Props = {
  app: Application,
  position: number
};

class TopFreeAppItem extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.index}>
          {this.props.position + 1}
        </Text>
        <Image
          style={styles.appIcon}
          borderRadius={this.props.position % 2 == 0 ? 10 : 25}
          source={{ uri: this.props.app['im:image'][1].label }}
        />
        <View style={styles.appInfo}>
          <Text style={styles.appName} numberOfLines={2} ellipsizeMode="tail">
            {this.props.app['im:name'].label}
          </Text>
          <Text style={styles.appCat}>
            {this.props.app.category.attributes.label}
          </Text>
          {this.props.app.averageUserRating
            ? <View style={styles.appRate}>
                <StarRating
                  starColor={'orange'}
                  emptyStarColor={'orange'}
                  disabled={true}
                  maxStars={5}
                  starSize={BaseStyles.smallFont}
                  rating={this.props.app.averageUserRating}
                />
                <Text style={styles.appRateCount}>
                  {`(${this.props.app.userRatingCount})`}
                </Text>
              </View>
            : <View />}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 80,
    flexDirection: 'row',
    backgroundColor: BaseStyles.white,
    justifyContent: 'center',
    padding: BaseStyles.layoutPadding,
    alignItems: 'center'
  },
  index: {
    width: 35,
    textAlign: 'center',
    fontSize: BaseStyles.largeFont,
    color: BaseStyles.darkGray
  },
  appIcon: {
    width: 55,
    height: 55,
    marginLeft: BaseStyles.layoutPadding
  },
  appInfo: {
    flex: 1,
    marginLeft: BaseStyles.layoutPadding
  },
  appName: {
    fontSize: BaseStyles.mediumFont,
    color: BaseStyles.darkGray
  },
  appCat: {
    fontSize: BaseStyles.smallFont,
    color: BaseStyles.darkGray,
    marginTop: BaseStyles.smallPadding
  },
  appRate: {
    flexDirection: 'row',
    marginTop: BaseStyles.smallPadding,
    alignItems: 'center'
  },
  appRateCount: {
    fontSize: BaseStyles.smallFont,
    color: BaseStyles.darkGray,
    marginLeft: BaseStyles.smallPadding,
    textAlign: 'center'
  }
});

module.exports = TopFreeAppItem;
