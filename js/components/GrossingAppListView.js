/**
 * @flow
 */

'use strict';

import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  ActivityIndicator
} from 'react-native';
import BaseStyles from '../BaseStyles';
import type { Application } from '../models/Application';
import TopGrossingAppItem from './TopGrossingAppItem';

type Props = {
  refreshing: boolean,
  topGrossingApps: Array<Application>
};

type State = {
  page: number
};

class GrossingAppListView extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      page: 1
    };
  }
  render() {
    return (
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={this.props.topGrossingApps}
        horizontal={true}
        keyExtractor={this._keyExtractor}
        refreshing={this.props.topGrossingApps !== null && this.props.refreshing}
        renderItem={this._renderItem}
      />
    );
  }

  _keyExtractor = (item: Application) => item.id.attributes['im:id'];
  _renderItem = ({ item, index }) => <TopGrossingAppItem app={item} />;
}

module.exports = GrossingAppListView;
