/**
 * @flow
 */

'use strict';

import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  ActivityIndicator
} from 'react-native';
import BaseStyles from '../BaseStyles';
import type { Application } from '../models/Application';
import TopFreeAppItem from './TopFreeAppItem';
import EmptyListView from './EmptyListView';
import GrossingAppListView from './GrossingAppListView';

type Props = {
  onRefresh: () => void,
  refreshing: boolean,
  topFreeApps: Array<Application>,
  topGrossingApps: Array<Application>
};

type State = {
  page: number
};

class AppListView extends React.Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      page: 1
    };
  }
  render() {
    return (
      <FlatList
        initialNumToRender={10}
        data={
          this.props.topFreeApps
            ? this.props.topFreeApps.slice(0, this.state.page * 10)
            : []
        }
        onEndReached={() =>
          this.setState({
            page: this.state.page < 10 ? this.state.page + 1 : this.state.page
          })}
        onRefresh={this.props.onRefresh}
        refreshing={this.props.topFreeApps !== null && this.props.refreshing}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        ItemSeparatorComponent={this._itemSeparatorComponent}
        ListEmptyComponent={this._ListEmptyComponent}
        ListHeaderComponent={this._ListHeaderComponent}
      />
    );
  }

  _keyExtractor = (item: Application) => item.id.attributes['im:id'];

  _renderItem = ({ item, index }) =>
    <TopFreeAppItem app={item} position={index} />;

  _itemSeparatorComponent = () => <View style={styles.separator} />;
  _ListEmptyComponent = () =>
    <EmptyListView refreshing={this.props.refreshing} />;
  _ListHeaderComponent = () =>
    ((this.props.topGrossingApps != null && this.props.topGrossingApps.length>0)?
    <View>
      <Text style={styles.recommended}>Recommended</Text>
      <GrossingAppListView
        refreshing={this.props.refreshing}
        topGrossingApps={this.props.topGrossingApps}
      />
    </View>:<View/>);
}

const styles = StyleSheet.create({
  separator: {
    height: BaseStyles.line,
    backgroundColor: BaseStyles.gray
  },
  recommended: {
    fontSize: BaseStyles.largeFont,
    color: BaseStyles.darkGray,
    marginLeft: BaseStyles.layoutPadding,
    marginTop: BaseStyles.layoutPadding
  }
});

module.exports = AppListView;
