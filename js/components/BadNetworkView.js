/**
 * @flow
 */

'use strict';

import React from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import BaseStyles from '../BaseStyles';

type Props = {
  onRetry: () => void
};

class BadNetworkView extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Cannot connect to App Store</Text>
        <TouchableOpacity onPress={this.props.onRetry}>
          <Text style={styles.btn}>Retry</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('window').height - BaseStyles.actionBarHeight,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    textAlign: 'center',
    fontSize: BaseStyles.largeFont
  },
  btn: {
    textAlign: 'center',
    fontSize: BaseStyles.mediumFont,
    padding: BaseStyles.smallPadding,
    borderWidth: BaseStyles.line,
    borderRadius: 3,
    marginTop:BaseStyles.layoutPadding
  }
});

module.exports = BadNetworkView;
