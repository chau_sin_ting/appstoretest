/**
 * @flow
 */

'use strict';

import React from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import BaseStyles from '../BaseStyles';

type Props = {
  onChangeText: (text: string) => void
};

class SearchBar extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          onChangeText={this.props.onChangeText}
          returnKeyType="search"
          placeholder="Search"
          underlineColorAndroid="transparent"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: BaseStyles.actionBarHeight,
    flexDirection: 'row',
    // width: WIDTH,
    backgroundColor: BaseStyles.lightGray,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: BaseStyles.statusBarHeight,
    borderBottomColor: BaseStyles.darkGray,
    borderBottomWidth: BaseStyles.line
  },
  textInput: {
    flex: 1,
    height: BaseStyles.searchBarHeight,
    fontSize: BaseStyles.mediumFont,
    margin: BaseStyles.layoutPadding,
    backgroundColor: BaseStyles.gray,
    borderRadius: 4,
    textAlign: 'center'
  }
});

module.exports = SearchBar;
