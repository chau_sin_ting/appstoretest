/**
 * @flow
 */

'use strict';

import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import BaseStyles from '../BaseStyles';
import type { Application } from '../models/Application';

type Props = {
  app: Application
};

class TopGrossingAppItem extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.appIcon}
          borderRadius={15}
          source={{ uri: this.props.app['im:image'][1].label }}
        />
        <Text style={styles.appName} numberOfLines={2} ellipsizeMode="tail">
          {this.props.app['im:name'].label}
        </Text>
        <Text style={styles.appCat}>
          {this.props.app.category.attributes.label}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 150,
    width: 90,
    backgroundColor: BaseStyles.white,
    padding: BaseStyles.layoutPadding,
  },
  appIcon: {
    width: 70,
    height: 70,
  },
  appName: {
    fontSize: BaseStyles.mediumFont,
    color: BaseStyles.darkGray,
    marginTop: BaseStyles.smallPadding
  },
  appCat: {
    fontSize: BaseStyles.smallFont,
    color: BaseStyles.darkGray,
    marginTop: BaseStyles.smallPadding
  }
});

module.exports = TopGrossingAppItem;
