export default {
  timeout: 5000,
  baseURL: 'https://itunes.apple.com/hk',
  getTopFree: '/rss/topfreeapplications/limit=100/json',
  getTopGrossing: '/rss/topgrossingapplications/limit=10/json',
  lookUp: '/lookup?id='
};
