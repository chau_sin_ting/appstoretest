/**
 * @flow
 */
import type { Application } from '../models/Application';

export type Action =
  | { type: 'LOADING_TOP_FREE' }
  | { type: 'GOT_TOP_FREE', apps: Array<Application> }
  | { type: 'GETT_TOP_FREE_FAILED' }
  | { type: 'LOADING_TOP_GROSSING' }
  | { type: 'GOT_TOP_GROSSING', apps: Array<Application> }
  | { type: 'GET_TOP_GROSSING_FAILED' };

export type Dispatch = (action: Action | ThunkAction | Array<Action>) => any;
export type GetState = () => Object;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
