/**
 * @flow
 */

import type { ThunkAction, Dispatch, Action } from './types';
import axios from 'axios';
import apiSettings from '../APISettings';
import type { Application } from '../models/Application';

export const LOADING_TOP_GROSSING = 'LOADING_TOP_GROSSING';
export const GOT_TOP_GROSSING = 'GOT_TOP_GROSSING';
export const GET_TOP_GROSSING_FAILED = 'GET_TOP_GROSSING_FAILED';


export function fetchTopGrossingApps(): ThunkAction {
  return (dispatch: Dispatch) => {
    dispatch(loadingTopGrossing())
    const instance = axios.create({
      baseURL: apiSettings.baseURL,
      timeout: apiSettings.timeout,
    });
    return instance
      .get(apiSettings.getTopGrossing)
      .then(function(response) {
        console.log(response);
        dispatch(gotTopGrossingApps(response.data.feed.entry))
      })
      .catch(function(error) {
        console.log(error);
        dispatch(getTopGrossingFailed())
      });
  };
}

function loadingTopGrossing(): Action {
  return {
    type: LOADING_TOP_GROSSING
  };
}

function gotTopGrossingApps(apps: Array<Application>): Action {
  return {
    type: GOT_TOP_GROSSING,
    apps: apps
  };
}

function getTopGrossingFailed(): Action {
  return {
    type: GET_TOP_GROSSING_FAILED,
  };
}
