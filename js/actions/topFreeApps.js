/**
 * @flow
 */

import type { ThunkAction, Dispatch, Action } from './types';
import axios from 'axios';
import apiSettings from '../APISettings';
import type { Application } from '../models/Application';

export const LOADING_TOP_FREE = 'LOADING_TOP_FREE';
export const GOT_TOP_FREE = 'GOT_TOP_FREE';
export const GOT_APP_DETAILS = 'GOT_APP_DETAILS';
export const GETT_TOP_FREE_FAILED = 'GETT_TOP_FREE_FAILED';

export function fetchTopFreeApps(): ThunkAction {
  return (dispatch: Dispatch) => {
    dispatch(loadingTopFree());
    const instance = axios.create({
      baseURL: apiSettings.baseURL,
      timeout: apiSettings.timeout
    });
    return instance
      .get(apiSettings.getTopFree)
      .then(function(response) {
        console.log(response);
        dispatch(fetchAppDetail(response.data.feed.entry));
      })
      .catch(function(error) {
        console.log(error);
        dispatch(getTopFreeFailed());
      });
  };
}

export function fetchAppDetail(apps: Array<Application>): ThunkAction {
  return (dispatch: Dispatch) => {
    const instance = axios.create({
      baseURL: apiSettings.baseURL,
      timeout: apiSettings.timeout
    });
    let ids = '';
    for (var i = 0; i < apps.length; i++) {
      ids = ids + apps[i].id.attributes['im:id'];
      if (i !== apps.length - 1) ids = ids + ',';
    }
    return instance
      .get(apiSettings.lookUp + ids)
      .then(function(response) {
        console.log(response);
        let details = response.data.results;
        for (var i = 0; i < details.length; i++) {
          if (apps) {
            apps[i].averageUserRating = details[i].averageUserRating;
            apps[i].userRatingCount = details[i].userRatingCount;
          }
        }
        dispatch(gotTopFree(apps));
      })
      .catch(function(error) {
        console.log(error);
        dispatch(getTopFreeFailed());
      });
  };
}

function loadingTopFree(): Action {
  return {
    type: LOADING_TOP_FREE
  };
}

function gotTopFree(apps: Array<Application>): Action {
  return {
    type: GOT_TOP_FREE,
    apps: apps
  };
}


function getTopFreeFailed(): Action {
  return {
    type: GETT_TOP_FREE_FAILED
  };
}
